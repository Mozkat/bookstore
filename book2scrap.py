#!/usr/bin/env python3
import requests
import csv
import os
from os.path import exists
from bs4 import BeautifulSoup

book_site_url = "http://books.toscrape.com/catalogue/"
header_csv = ['product_page_url',
        'universal_product_code',
        'title','price_including_tax',
        'price_excluding_tax',
        'number_available',
        'product_description',
        'category',
        'review_rating',
        'image_url']
csv_base_name = 'bookstore_data_'

rating_map = {'One': 1, 'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5}

# Use of requests module to get and retrieve resource data from URL:
def fetchURL(URL):
    webpage = requests.get(URL)
    return webpage.content

# Create or append data to CSV file:
def loadCSV(csv_file_name, product_url, product_upc, product_title, product_price_vat, product_price_novat, product_availability, product_desc, product_categ, product_review_rating, product_img_url):
    csv_file_name = './' + csv_file_name + '.csv'
    if not exists(csv_file_name):
        with open(csv_file_name, 'w', encoding='UTF8', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(header_csv)
            csvwriter.writerow([product_url, product_upc, product_title, product_price_vat, product_price_novat, product_availability, product_desc, product_categ, product_review_rating, product_img_url])
    else:
        with open(csv_file_name, 'a', encoding='UTF8', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow([product_url, product_upc, product_title, product_price_vat, product_price_novat, product_availability, product_desc, product_categ, product_review_rating, product_img_url])

def downloadIMG(img_src, img_upc_name, img_extension):
    req_book_image = fetchURL(img_src)
    if not exists('./books_img'):
       os.makedirs('./books_img')
    if req_book_image:
        with open('./books_img/'+ img_upc_name + img_extension, 'wb') as imgfile:
            imgfile.write(req_book_image)

def parseURL(contextURL):
    base_url = 'http://books.toscrape.com/'
    soupURL = BeautifulSoup(contextURL, 'html.parser')
    books_product = soupURL.find_all('article', class_='product_pod')
    next_page = soupURL.find('li', class_='next')
    for book in books_product:
        book_url = book_site_url + book.find_all('a')[1]['href']
        #product url:
        print(book_url)
        #product title:
        book_title = book.find_all('a')[1]['title']
        #product image url:
        book_img_url = base_url + book.find('img')['src'].replace('../', '')
        #follow product url to get book details:
        soup_book_page = BeautifulSoup(fetchURL(book_url), 'html.parser')
        #book desc:
        book_desc = soup_book_page.find_all('p')[3].text
        #book upc:
        book_upc = soup_book_page.find_all('td')[0].text
        #book price_novat:
        book_price_novat = soup_book_page.find_all('td')[2].text
        #book price_vat:
        book_price_vat = soup_book_page.find_all('td')[3].text
        #book availability:
        book_availability = soup_book_page.find_all('td')[5].text
        #book category:
        book_category = soup_book_page.find('ul', class_='breadcrumb').find_all('a')[2].text
        #book rating:
        book_rating = rating_map[soup_book_page.find('div', class_='col-sm-6 product_main').find_all('p')[2]['class'][1]]
        #push data to CSV file:
        loadCSV(csv_base_name + book_category.replace(' ', '_').lower(), book_url, book_upc, book_title, book_price_vat, book_price_novat, book_availability, book_desc, book_category, book_rating, book_img_url)
        #download and save book image:
        downloadIMG(book_img_url, book_upc, '.jpg')
    if next_page is None:
        exit()
    else:
        print('Next book page is: ' + book_site_url + str(next_page.a.get('href')))
        parseURL(fetchURL(book_site_url + str(next_page.a.get('href'))))


parseURL(fetchURL(book_site_url+'page-1.html'))
