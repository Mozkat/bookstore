# Book2Store Project Repo

## How-to create and activate your virtual environment using requirements.txt and Python3 ?
First of all, open-up your terminal emulator.
Type-in: "python3 --version" without double quotes to check you have correctly installed Python3

Create your virtual working environment with this command in terminal:
python3 -m venv /path/to/env_name

Activate your newly created virtual environment with this command in terminal:
source /path/to/env_name/bin/activate

Terminal prompt should print:
(env_name) $

Now install dependencies for this project with this command in terminal:
python3 -m pip install -r requirements.txt

The requirements.txt file you downloaded from this project repo will install and config dependencies for running this program properly

Once done, run in terminal the following command:
(env_name) $./book2scrap.py

Enjoy !
